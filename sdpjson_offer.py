import sdp_transform
import asyncio
import json


class EchoClientProtocol:
    def __init__(self, sdp, on_con_lost):
        self.sdp = sdp
        self.on_con_lost = on_con_lost
        self.transport = None

    def connection_made(self, transport):
        self.transport = transport
        print('Send:', self.sdp)
        self.transport.sendto(self.sdp.encode())

    def datagram_received(self, data, addr):
        print("Received:", data.decode())

        print("Close the socket")
        self.transport.close()

    def error_received(self, exc):
        print('Error received:', exc)

    def connection_lost(self, exc):
        print("Connection closed")
        self.on_con_lost.set_result(True)


async def main():
    # Get a reference to the event loop as we plan to use
    # low-level APIs.
    loop = asyncio.get_running_loop()

    on_con_lost = loop.create_future()
    with open("sdp.json", "r") as jfile:
        sdp = json.load(jfile)
    json_sdp = json.dumps({"type": "offer", "sdp": sdp_transform.write(sdp)})

    transport, protocol = await loop.create_datagram_endpoint(
        lambda: EchoClientProtocol(json_sdp, on_con_lost),
        remote_addr=('127.0.0.1', 9991))

    try:
        await on_con_lost
    finally:
        transport.close()


asyncio.run(main())
